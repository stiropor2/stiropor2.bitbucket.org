
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 var predpripravljeniEHRidji = ["9f697d53-339e-4bcc-a9c0-17391df6a1a9", "0c4632ff-d425-432d-82db-9e77ddda4251", "b98de23c-0c7b-4e3a-b8d7-bbcfa1555b9b"];
 
function pozeniGenerator() {
    generirajPodatke(1);
    generirajPodatke(2);
    generirajPodatke(3);
}
function generirajPodatke(stPacienta) {
  var sessionId = getSessionId();
    ehrId = "";

    switch(stPacienta) {
        case 1:
            //var ime = "Zdrav";
            //var priimek = "Kodren";
            datumcek = "1940-05-11T07:22";
            var visina = "180";
            var teza = "77";
            var temperatura = "37";
            var sTlak = "90";
            var dTlak = "60";
            updatajEHR(predpripravljeniEHRidji[0], datumcek, visina, teza, temperatura, sTlak, dTlak);
            break;
        case 2:
            //var ime = "Navaden";
            //var priimek = "Povprecnez";
            datumcek = "1940-05-11T07:22";
            var visina = "180";
            var teza = "77";
            var temperatura = "37";
            var sTlak = "90";
            var dTlak = "60";
            updatajEHR(predpripravljeniEHRidji[1], datumcek, visina, teza, temperatura, sTlak, dTlak);
            break;
        case 3:
            //var ime = "Zdravje";
            //var priimek = "Pesami";
            var datumcek = "1940-05-11T07:22";
            var visina = "180";
            var teza = "77";
            var temperatura = "37";
            var sTlak = "90";
            var dTlak = "60";
            updatajEHR(predpripravljeniEHRidji[2], datumcek, visina, teza, temperatura, sTlak, dTlak);
            break;
    }
    
  // TODO: Potrebno implementirati

  return ehrId;
}

//tle za generator updata
function updatajEHR(ehrId, datumInUra, visina, teza, temperatura, sTlak, dTlak) {
    console.log("zgodilo se je!");
    var nasicenostKrviSKisikom = 1;
    		    var merilec = "nivazno";
    		    var sessionId = getSessionId();
    		    $.ajaxSetup({
    		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": visina,
		    "vital_signs/body_weight/any_event/body_weight": teza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": temperatura,
		   	"vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": dTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#pokaziSporocilo").append("</br></br> \
              Vnos podatkov je bil uspešen: EHR ID: " + ehrId + "</br><small>" +
              res.meta.href + "</small>");
		    },
		    error: function(err) {
		    	$("#pokazisporocilo2").html(
            "Napaka: '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
});
}


function kreirajnovEHR() {
    var sessionId = getSessionId();
    //console.log("trala!");
    var ime = $("#ime").val();
    var priimek = $("#priimek").val();
    var datum = $("#datumRojstva").val();
    
    if(ime.length == 0 || priimek.length == 0 || datum.length == 0) {
        $("#pokaziSporocilo").html("\
  <strong>Napaka!</strong> Prosim pravilno vnesite podatke in poskusite ponovno!\
");

    } else {
$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datum,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#pokaziSporocilo").html("Uspešno! EHR ID: '" +
                          ehrId + "'.</span>");
		                    $("#ehrid").val(ehrId); //tle treba se spremenit
		                }
		            },
		            error: function(err) {
		            	$("#pokaziSporocilo").html("Napaka: '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
}
    
}
function dodajEhrZapis() {
    console.log("tukej smo");
    var sessionId = getSessionId();
    //console.log("trala!");
    var ehrId = $("#ehrid").val();
    var dInUra = $("#datumInUra").val();
    var tVisina = $("#tVisina").val();
    var tTeza = $("#tTeza").val();
    var tTemp = $("#tTemp").val();
    var tSist = $("#tSist").val();
    var tDiast = $("#tDiast").val();
    
    if(ehrId.length == 0 || dInUra.length == 0 || tVisina.length == 0 || tTeza.length == 0 || tTemp.length == 0 || tSist.length == 0 || tDiast.length == 0) {
        $("#pokazisporocilo2").html("Prosim vnesite vse potrebne podatke!");
    } else {
        updatajEHR(ehrId, dInUra, tVisina, tTeza, tTemp, tSist, tDiast);
        $("#pokazisporocilo2").html("Podatki uspesno shranjeni! Podrobnosti so prikazane v levem okencu!");
    }
}


function prikaziValue(stPacienta) {
    var sessionId = getSessionId();
    //console.log(predpripravljeniEHRidji[stPacienta]);
    document.getElementById('vnosEHRid').value = predpripravljeniEHRidji[stPacienta];
    document.getElementById('ehrid').value = predpripravljeniEHRidji[stPacienta];
    document.getElementById('ehrVnosek').value = predpripravljeniEHRidji[stPacienta];
    var ehrId = predpripravljeniEHRidji[stPacienta];
    	$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				document.getElementById('ime').value = party.firstNames;
				document.getElementById('priimek').value = party.lastNames;
				document.getElementById('datumRojstva').value = party.dateOfBirth;
			console.log("tle smo");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
});
    
}

function pobrisiSporocila(){
    $("#pokazisporocilo2").html("");
    $("#pokaziSporocilo").html("");
}

function reversePoizvedba() {
   var sessionId = getSessionId();
    var ehrId = $("#vnosEHRid").val();
    
    if (ehrId.length == 0) {
        $("#pokaziSporocilo3").html("Prosim vnesite EHR ID!");
    } else {
        		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#pokaziSporocilo3").html("Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil " + party.dateOfBirth);
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
});
    }
   
}

function preveriZdravje() {
      //$("#branjeRezultatov").html("Out of time...incomplete");
      var ehrId = $("#ehrVnosek").val();

      var sessionId = getSessionId();


  if (ehrId.length == 0) {
      console.log("triggerddd!");
    $("#branjeRezultatov").html("Neuspesno! Vnesite EHR ID!");
  } else {
    $.ajax({
      url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
      type: 'GET',
      headers: {"Ehr-Session": sessionId},
      success: function (data) {
        var party = data.party;
        $("#branjeRezultatov").html("<br/>Pridobivam podatke o telesni teži pacienta " + party.firstNames +
          " " + party.lastNames + "'<br/>");
        if (13 != 37) { //fail
          $.ajax({
            url: baseUrl + "/view/" + ehrId + "/" + "weight",
            type: 'GET',
            headers: {"Ehr-Session": sessionId},
            success: function (res) {
              if (res.length > 0) {
                var results =  "<table class='table table-bordered " +
                              "table-hover'><tr><th>Čas meritve</th>" +
                              "<th>Telesna teža</th></tr>";
                for (var i in res) {
                  results += "<tr><td>" + res[i].time +
                  "</td><td class='text-right'>" + res[i].weight +
                  " " + res[i].unit + "</td>";
                }
                results += "</table>";
                $("#branjeRezultatov").append(results);
              }
              else {
                $("#branjeRezultatov").html("Ni podatkov!</span>");
              }

              var x = [];
              var y = [];
              for (var i in res) {
                x.push(res[i].time);
                y.push(res[i].weight);
              }

              var data = [
                {
                  x: x,
                  y: y,
                  type: 'scatter',
                marker: {
                  color: "#F78181",
                },
                }
              ];

              var dezign = {
              title: "Graf meritev telesnih tež pacienta",
              xaxis: {
                title: 'Datum merjenja',
                showgrid: false,
                zeroline: false
              },
              yaxis: {
                title: "Telesna teža",
                showline: false
              }
            }

            Plotly.newPlot('graph', data, dezign);

          },


            error: function() {
              $("#preberiMeritveVitalnihZnakovSporocilo").html(
                "<br><span class='obvestilo label label-danger fade-in'>Napaka '" +
                JSON.parse(err.responseText).userMessage + "'!");
            }
          });
    
        }
      },
      error: function(err) {
        $("#sporociloNapaka").html(
          "Napaka: " +
          JSON.parse(err.responseText).userMessage);
      }
    });
  }
}

function openMaps(tip) {
    if(tip == 0) {
        window.open("map.html"); 
    } else {
        window.open("map2.html"); 
    }
}
    




// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
